﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour {

	public float speed = 0.1f;
	float border = 2.37f;
	float moveInput;

	gameController gameContr;

	void Start()
	{
		gameContr = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<gameController> ();
	}

	void Update () 
	{
		gameContr.UpdateLabels ();
		moveInput = Input.GetAxis ("Horizontal");
		if (gameContr.canPlay && moveInput != 0) {
			if ((transform.position.x > -border && moveInput < 0) || (transform.position.x < border && moveInput > 0)) {
				transform.position = new Vector2 (transform.position.x + moveInput * speed, transform.position.y);
				if (!gameState.IsGame()) 
				{
					GameObject.FindGameObjectWithTag ("ball").transform.position = new Vector2 (transform.position.x, GameObject.FindGameObjectWithTag ("ball").transform.position.y);
				}
			}
		}
		if (gameContr.canPlay && Input.GetKeyDown("space") && !gameState.IsGame()) 
		{
			GameObject.FindGameObjectWithTag ("ball").GetComponent<mover>().StartGame ();
			gameState.StartGame ();
			gameContr.StartTimer ();
		}
	}
}
