﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class gameState{

	public static int score = 0;
	public static bool game = false;

	public static int GetScore()
	{
		return score;
	}

	public static void StartGame()
	{
		game = true;
	}

	public static bool IsGame()
	{
		return game;
	}

	public static void StopGame()
	{
		game = false;
	}
}
