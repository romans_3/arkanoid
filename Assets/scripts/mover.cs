﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mover : MonoBehaviour {

	public float inpulseForce = 3f;
	[SerializeField]
	float angle = 0;

	public void StartGame () {
		Vector2 diraction = new Vector2 (Mathf.Cos (angle), Mathf.Sin (angle));
		GetComponent<Rigidbody2D> ().AddForce (diraction * inpulseForce, ForceMode2D.Impulse);
	}
}
