﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class brickController : MonoBehaviour {

	int life = 1;

	void OnCollisionEnter2D(Collision2D collision){
		life--;
		gameState.score++;
		if (life == 0)
			Destroy (transform.gameObject);
		else
			SetColor ();
	}
		
	public void SetLife(int value){
		life = value;
		SetColor ();
	}

	private void SetColor()
	{
		transform.GetComponent<SpriteRenderer> ().color = new Color (1.0f - (life - 1) * 0.2f, 1.0f - (life - 1) * 0.2f, 1.0f - (life - 1) * 0.2f);
	}
}
