﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameController : MonoBehaviour {

	public Text pointsLabel;
	public Text timerLabel;
	public Text status;
	public Text finallyScore;
	public Text finallyScoreTxt;

	public Image startScreen;
	public Image statusPanel;


	public float timerDelay = 10.0f;
	public bool canPlay = false;

	private int minCountBricks = 10;
	private int maxCountBricks = 30;
	private int countBricks = 0;
	private int bricksLifes = 0;

	private Vector3 ballPosition = new Vector3 ();
	private Vector3 playerPosition = new Vector3 ();

	public GameObject brick;
	public GameObject[] bricks;
	public GameObject bricksSpawner;

	Timer gameTimer;

	void Start () 
	{
		gameTimer = gameObject.AddComponent<Timer> ();
		CreateLevel ();
		ballPosition = GameObject.FindGameObjectWithTag ("ball").transform.position;
		playerPosition = GameObject.FindGameObjectWithTag ("Player").transform.position;
	}
		
	void Update () 
	{
		if (gameState.IsGame() && gameTimer.Finished) 
		{
			MoveDownLevel ();
			StartTimer ();
		}
	}

	private void CreateLevel()
	{
		countBricks = Random.Range (minCountBricks, maxCountBricks);
		float y_step = 4.0f;
		bricksLifes = 0;
		for (int value = 0; value < countBricks; value++) 
		{
			GameObject tmpBrick = (GameObject)Instantiate(brick);
			int brickLife = Random.Range (1, 4);
			bricksLifes += brickLife;
			tmpBrick.GetComponent<brickController> ().SetLife (brickLife);
			tmpBrick.transform.SetParent(bricksSpawner.transform);
			if (value != 0 && value % 5 == 0) 
			{
				y_step -= 0.5f;
			}
			tmpBrick.transform.position = new Vector2 (-2.2f + (value % 5) * 1.1f, y_step);
			//print ((-2.2f + (value % 5) * 1.1f) + "  " + y_step);
		}
	}

	private void MoveDownLevel()
	{
		bricks = GameObject.FindGameObjectsWithTag ("brick");
		foreach (GameObject tmpBrick in bricks)
		{
			tmpBrick.transform.position = new Vector2 (tmpBrick.transform.position.x, tmpBrick.transform.position.y - 1.0f);
			if (tmpBrick.transform.position.y - 1.0f < -4.5f)
				GameOver (false);
		}
	}

	private void DestroyBricks()
	{
		bricks = GameObject.FindGameObjectsWithTag ("brick");
		foreach (GameObject tmpBrick in bricks)
		{
			Destroy (tmpBrick);
		}
	}

	public void StartTimer()
	{
		if (gameTimer) 
		{
			gameTimer.Duration = timerDelay;
			gameTimer.Run ();
		}
	}
	public void OnStartBtnPressed()
	{
		//CreateLevel ();
		gameState.score = 0;
		GameObject.FindGameObjectWithTag ("ball").GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.None;
		startScreen.gameObject.SetActive(false);
		statusPanel.gameObject.SetActive(true);
		canPlay = true;
		UpdateLabels ();
	}

	public void OnSettingsBtnPressed()
	{
		print ("settings");
	}

	public void OnSoundBtnPressed()
	{
		print ("sound");
	}

	public void OnExitBtnPressed()
	{
		Application.Quit ();
	}

	public void GameOver(bool win)
	{
		GameObject.FindGameObjectWithTag ("ball").GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezePosition;
		GameObject.FindGameObjectWithTag ("ball").transform.position = ballPosition;
		GameObject.FindGameObjectWithTag ("Player").transform.position = playerPosition;
		DestroyBricks ();
		CreateLevel ();
		gameTimer.Stop ();
		canPlay = false;
		gameState.StopGame ();
		startScreen.gameObject.SetActive(true);
		statusPanel.gameObject.SetActive(false);
		status.gameObject.SetActive(true);
		finallyScore.gameObject.SetActive(true);
		finallyScoreTxt.gameObject.SetActive(true);
		finallyScore.text = gameState.GetScore().ToString();
		if (win) 
		{
			status.text = "You win!";
			status.color = new Color (0, 255, 0);
		} 
		else 
		{
			status.text = "You lose!";
			status.color = new Color (255, 0, 0);
		}
	}

	public void UpdateLabels()
	{
		pointsLabel.text = gameState.GetScore().ToString();
		timerLabel.text = Mathf.Round(timerDelay - gameTimer.GetElapsedSeconds()).ToString();
		if (gameState.GetScore () == bricksLifes)
			GameOver (true);
	}
}
